/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cdi;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import beans_a.*;
import beans_b.*;
import entity_a.*;
import entity_b.*;
import java.util.List;


/**
 *
 * @author пк
 */
@Named("bean")
@SessionScoped
public class CdiBean implements Serializable {
    @EJB
    private TransactionBeanLocal transactionBean;
    
    @EJB
    private ClientFacade clientFacade;
    
    @EJB
    private ContactFacade contactFacade;
    
    private Client client;
    private Contact contact;
    private String name ="";
    private String surname ="";
    private String address ="";
    private String phone ="";

    public List<Client> getAllClients()
    {
        return clientFacade.findAll();
    }
    
    public List<Contact> getAllContacts()
    {
        return contactFacade.findAll();
    }
    public String addToSecondDatabase()
    {
        contact = new Contact(phone, address,0);
        contactFacade.createContact(contact);
        return "/index.xhtml";
    }
    
    public String executeTransactionSuccess()
    {
        client = new Client(name, surname);
      //  System.out.println("new client"+client.getName()+" "+ client.getSurname());
        contact = new Contact(phone, address, 0);
       // System.out.println("new contact"+ contact.getAddress()+" "+contact.getPhone()+" "+ contact.getIdClient());
        transactionBean.ex1(client, contact);
        return "/index.xhtml";
        
    }
    
    public String rollbackFirstDB()
    {
        client = new Client(name, surname);
        contact = new Contact(phone, address, 0);
        transactionBean.ex2(client, contact);
        return "/index.xhtml";
    }
    
    public String throwExeptionSecondDB()
    {
        client = new Client(name, surname);
        contact = new Contact(phone, address, 0);
        transactionBean.ex3(client, contact);
        return "/index.xhtml";
    }
    
    public String executeTransactionNotSupported()
    {
        client = new Client(name, surname);
        contact = new Contact(phone, address, 0);
        transactionBean.ex4(client, contact);
        return "/index.xhtml";
    }
    
    public String executeTransactionNewContext()
    {
        client = new Client(name, surname);
        contact = new Contact(phone, address, 0);
        transactionBean.ex5(client, contact);
        return "/index.xhtml";
    }
    
    public TransactionBeanLocal getTransactionBean() {
        return transactionBean;
    }

    public void setTransactionBean(TransactionBeanLocal transactionBean) {
        this.transactionBean = transactionBean;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
}
