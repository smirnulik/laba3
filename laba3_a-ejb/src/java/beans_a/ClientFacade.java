/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans_a;

import entity_a.Client;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author пк
 */
@Stateless
public class ClientFacade extends AbstractFacade<Client> {
  
    @PersistenceContext(unitName = "laba3_a-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Resource
    private SessionContext sessionContext;
    
    public ClientFacade() {
        super(Client.class);
    }
    
    public Integer createClient(Client client)
    {
        return create(client);
    }
    
    public Client rollbackTransaction(Client client)
    {
        client = getEntityManager().find(Client.class, client.getIdClient());
        sessionContext.setRollbackOnly();
        return client;
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void createClientNewTrasaction(Client client)
    {
        create(client);
    }
}
