/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans_a;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import entity_a.*;
import entity_b.*;
import beans_a.*;
import beans_b.*;
import javax.persistence.EntityManager;
/**
 *
 * @author пк
 */
@Stateless
public class TransactionBean implements TransactionBeanLocal {
    @EJB
    private ClientFacade clientFacade;
    
    @EJB
    private ContactFacade contactFacade;
    
    
    @Override
    public void ex1(Client client, Contact contact)
    {
        int id=clientFacade.createClient(client);
        System.out.println("id="+id);
        contact.setIdClient(id);
        System.out.println("2 shag idclient from contact"+contact.getIdClient());
        contactFacade.createContact(contact);
        System.out.println("3 shag "+contact.toString());
    }
    @Override
    public void ex2(Client client, Contact contact)
    {
        contactFacade.create(contact);
        clientFacade.rollbackTransaction(client);
    }
    @Override
    public void ex3(Client client, Contact contact)
    {
        int id=clientFacade.createClient(client);
        contact.setIdClient(id);
        contactFacade.rollbackExeption(contact);
    }
    @Override
    public void ex4(Client client, Contact contact)
    {
         clientFacade.rollbackTransaction(client);
         contactFacade.createContactOnlyDB2(contact);
    }
    @Override
    public void ex5(Client client, Contact contact)
    {
        clientFacade.createClientNewTrasaction(client);
        contactFacade.rollbackExeption(contact);
    }        
            
    
    
}
