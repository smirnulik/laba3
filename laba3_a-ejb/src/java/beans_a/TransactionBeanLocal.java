/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans_a;

import javax.ejb.Local;
import entity_a.*;
import entity_b.*;
/**
 *
 * @author пк
 */
@Local
public interface TransactionBeanLocal {
    
    public void ex1(Client client, Contact contact);
    public void ex2(Client client, Contact contact);
    public void ex3(Client client, Contact contact);
    public void ex4(Client client, Contact contact);
    public void ex5(Client client, Contact contact);
}
